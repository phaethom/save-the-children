package com.engitronics.saveTheChildren.utils;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class EncryptDecryptTest {


    EncryptDecrypt encryptDecrypt;

    String iv;
    String messageToEncrypt;
    String secretKey;

    @BeforeEach
    public void init(){
         iv = "R344545GFFGhjj_@";//must be 16 bytes long
         messageToEncrypt="Hello World";
         secretKey = "Q£4$%&*)9000{}]";
        encryptDecrypt = new EncryptDecrypt();
    }

    @Test
    @SneakyThrows
    public void testEncryptAes() {
        String encryptedMessage = encryptDecrypt.encryptAes(messageToEncrypt,iv,secretKey);
        System.out.println(encryptedMessage);
        Assertions.assertNotEquals(encryptedMessage,messageToEncrypt);
    }

    @Test
    @SneakyThrows
    public void testDecryptAES() {
        String encryptedMessage = encryptDecrypt.encryptAes(messageToEncrypt,iv,secretKey);
        String decryptedMessageCopy = encryptDecrypt.decryptAES(encryptedMessage,iv,secretKey);
        Assertions.assertEquals(messageToEncrypt, decryptedMessageCopy);
    }
}