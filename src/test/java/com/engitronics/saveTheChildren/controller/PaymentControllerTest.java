package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.model.Payment;
import com.engitronics.saveTheChildren.service.PaymentService;
import com.engitronics.saveTheChildren.utils.PaymentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.mockito.Mockito.when;

@WebMvcTest
public class PaymentControllerTest {


    @Mock
    PaymentService paymentService;

    @InjectMocks
    PaymentController paymentController;

    Payment payment;

    @Before
    public void setUp() {
        RestAssuredMockMvc.standaloneSetup(this.paymentController);

        PaymentType paypal = PaymentType.PAYPAL;
        payment= new Payment();
        payment.setId(1L);
        payment.setPaymentTypeNumber("987654323456");
        payment.setPaymentType(paypal);
        payment.setAmount(25F);

    }



    @Test
    public void testGetById() {
        when(paymentService.retrieveById(payment.getId())).thenReturn(Optional.of(payment));
        ResponseEntity<Payment> response =  paymentController.getById(payment.getId());
        Assert.assertEquals(payment.getId(), response.getBody());


    }
}