package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.exceptions.NotFoundException;
import com.engitronics.saveTheChildren.model.User;
import com.engitronics.saveTheChildren.service.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    User userA;
    User userB;
    List<User> userList;


    @Before
    public void init(){
        Date date = new Date();
        Date date2 = new Date();

        userA = new User();
        userA.setId(45L);
        userA.setName("John");
        userA.setSurname("Doe");
        userA.setEmail("john@email.com");
        userA.setCreatedOn(date);

        userA = new User();
        userA.setId(33L);
        userA.setName("Maria");
        userA.setSurname("Alonzo");
        userA.setEmail("maria@email.com");
        userA.setCreatedOn(date2);

        userList = new ArrayList<>();
        userList.add(userA);
        userList.add(userB);
    }
    @After
    public void finalise(){
        userList.clear();
    }

    @Test
    public void testGetUserById(){
        when(userService.retrieveById(userA.getId())).thenReturn(Optional.of(userA));
        ResponseEntity<User> response =  userController.getById(userA.getId());
        Assert.assertEquals(userA, response.getBody());
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserByUnknownId() throws NotFoundException{
        when(userService.retrieveById(123L)).thenThrow(new NotFoundException("User inexistent!"));
        ResponseEntity<User> response =  userController.getById(userA.getId());
    }

    @Test
    public void testDeleteUser(){
        userService.deleteById(userA.getId());
        Mockito.verify(userService,Mockito.times(1)).deleteById(userA.getId());
    }



   @Test(expected = NotFoundException.class)
    public void DeleteUserFailTest()  throws NotFoundException{
        when(userService.retrieveById(123L)).thenThrow(new NotFoundException("User inexistent!"));
        ResponseEntity<User> response =  userController.getById(userA.getId());
    }


    @Test
    public void testGetAllUsers(){
        Pageable pageable = PageRequest.of(0, 2);
        Page<User> usrl = new PageImpl(userList, pageable,2);
        Mockito.when(userService.retrieveAll(pageable)).thenReturn(usrl);
        ResponseEntity<Page<User>> response = userController.getAllByPage(pageable);
        Assert.assertEquals(usrl,response.getBody());

   }

}
