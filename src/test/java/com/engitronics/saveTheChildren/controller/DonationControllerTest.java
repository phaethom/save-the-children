package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.model.Donation;
import com.engitronics.saveTheChildren.service.DonationService;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DonationControllerTest {

    @Mock
    DonationService donationService;

    @InjectMocks
    DonationController donationController;

    Donation donationA;
    Donation donationB;
    List<Donation> donationList;

    @Before
    public void setUp() {
        Date dt = new Date();
        donationA = new Donation();
        donationA.setId(12L);
        donationA.setName("Moroco appeal");
        donationA.setCountry("Moroco");
        donationA.setCreatedOn(dt);

        donationB = new Donation();
        donationB.setId(12L);
        donationB.setName("Moroco appeal");
        donationB.setCountry("Moroco");
        donationA.setCreatedOn(dt);

        donationList = new ArrayList<>();
        donationList.add(donationA);
        donationList.add(donationB);

    }

    @AfterEach
    public void tearDown() {
        donationList.clear();
    }

    @Test
    public void testCreate() {
    }

    @Test
    public void getAll() {
        Pageable pageable = PageRequest.of(0,2);
        Page<Donation> donations = new PageImpl(donationList, pageable,2);
        when(donationService.retrieveAll(pageable)).thenReturn(donations);
        ResponseEntity<Page<Donation>> response = donationController.getAll(pageable);
        Assertions.assertEquals(donations,response.getBody());
    }
}