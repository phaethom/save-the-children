package com.engitronics.saveTheChildren.service;

import com.engitronics.saveTheChildren.model.Donation;
import com.engitronics.saveTheChildren.repository.DonationRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DonationServiceTest {

    @Mock
    DonationRepository donationRepository;

    @InjectMocks
    DonationService donationService;

    Donation donationA;
    Donation donationB;
    Donation newDonation;
    List<Donation> donationList;

    @Before
    public void setUp() {
        Date dt = new Date();
        donationA = new Donation();
        donationA.setId(12L);
        donationA.setName("Moroco appeal");
        donationA.setCountry("Moroco");
        donationA.setCreatedOn(dt);


        donationB = new Donation();
        donationB.setId(34L);
        donationB.setName("Moroco appeal");
        donationB.setCountry("Moroco");
        donationA.setCreatedOn(dt);

        newDonation = new Donation();
        newDonation.setName("Moroco appeal");
        newDonation.setCountry("Moroco");
        newDonation.setCreatedOn(dt);



       donationList = new ArrayList<>();
       donationList.add(donationA);
       donationList.add(donationB);
    }

    @Test
    public void retrieveAll() {
        Pageable pageable = PageRequest.of(0,2);
        Page<Donation> donations = new PageImpl(donationList,pageable,2);
        when(donationRepository.findAll(pageable)).thenReturn(donations);
        Page<Donation> response = donationService.retrieveAll(pageable);

        Assert.assertEquals(donationList,response.get().collect(Collectors.toList()));
    }

    @Test
    public void testCreateOrUpdate() {
        when(donationRepository.save(newDonation)).thenReturn(donationA);
        Donation response = donationService.createOrUpdate(newDonation);
        Assertions.assertEquals(donationA, response);
    }
}