package com.engitronics.saveTheChildren.service;

import com.engitronics.saveTheChildren.model.User;
import com.engitronics.saveTheChildren.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    User userA;
    User userB;
    User newUser;
    List<User> userList;


    @Before
    public void setUp() {
        Date date = new Date();
        Date date2 = new Date();

        userA = new User();
        userA.setId(45L);
        userA.setName("John");
        userA.setSurname("Doe");
        userA.setEmail("john@email.com");
        userA.setCreatedOn(date);

        newUser = new User();
        newUser.setName("John");
        newUser.setSurname("Doe");
        newUser.setEmail("john@email.com");
        newUser.setCreatedOn(date);



        userB = new User();
        userB.setId(33L);
        userB.setName("Maria");
        userB.setSurname("Alonzo");
        userB.setEmail("maria@email.com");
        userB.setCreatedOn(date2);

        userList = new ArrayList<>();
        userList.add(userA);
        userList.add(userB);
    }



    @Test
   public void retrieveAll() {
        Pageable pageable = PageRequest.of(0, 2);

        Page<User> usrl = new PageImpl(userList, pageable,2);

        when(userRepository.findAll(pageable)).thenReturn(usrl);

        Page<User> response = userService.retrieveAll(pageable);

        Assert.assertEquals(userList,response.get().collect(Collectors.toList()));
    }

    @Test
    public void TestCreateOrUpdate() {

        when(userRepository.save(newUser)).thenReturn(userA);
        User response=userService.createOrUpdate(newUser);
        Assert.assertEquals(userA, response);
    }

    @Test
    public  void testRetrieveById() {
        when(userRepository.findById(userA.getId())).thenReturn(Optional.of(userA));
        Optional<User> response = userService.retrieveById(userA.getId());
        Assert.assertEquals(userA, response.get());

    }

    @Test
    public void testDeleteById() {
        userService.deleteById(userA.getId());
        Mockito.verify(userRepository, Mockito.times(1)).deleteById(userA.getId());

    }
}