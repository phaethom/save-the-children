SET sql_mode = '';

SET GLOBAL default_storage_engine = 'InnoDB';

create table if not exists user(
    id INT(5)  NOT NULL AUTO_INCREMENT,
    name VARCHAR(100)NOT NULL,
    surname VARCHAR(150) NOT NULL,
    email VARCHAR(100) NOT NULL,
    created_on DATETIME NOT NULL,
    primary key (id)
)engine=innodb;

create table if NOT EXISTS donation(
id INT(5) NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL,
country VARCHAR(50) NOT NULL,
created_on DATETIME NOT NULL,
primary key (id)
)engine=innodb;

create table if not exists payment(
    id INT(5) NOT NULL AUTO_INCREMENT,
    amount FLOAT NOT NULL,
    payment_type ENUM('PAYPAL','VISA','MASTERCARD','BITCOIN','AMERICAN_EXPRESS') NOT NULL,
    created_on DATETIME NOT NULL,
    user_id INT(5) NOT NULL,
    donation_id INT NOT NULL,
    primary key (id),
    payment_type_number VARCHAR(50),
    expiry_date DATETIME NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (donation_id) REFERENCES donation(id)
)engine=innodb;

