package com.engitronics.saveTheChildren.utils;

public class Constants {

    private Constants(){}

    public static final String LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String ERROR_DATE_PATTERN = "dd/MM/yyyy hh.mma";
}
