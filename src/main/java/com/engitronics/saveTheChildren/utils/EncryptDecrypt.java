package com.engitronics.saveTheChildren.utils;

import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class EncryptDecrypt {
    public String encryptAes(String message, String ivString, String secret) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String encryptedString = "err";
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(ivString.getBytes(StandardCharsets.UTF_8)); //must be 16 bytes long
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8),"AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec,ivParameterSpec);

            byte[] encrypted = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
            encryptedString = Base64.getEncoder().encodeToString(encrypted);

        }catch (Exception e){
            e.printStackTrace();
        }
        return encryptedString;
    }

    public String decryptAES(String encrypted, String initVector, String key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8),"AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec,ivParameterSpec);
        byte[] originalString = cipher.doFinal(Base64.getDecoder().decode(encrypted));
        return new String(originalString);
    }



}
