package com.engitronics.saveTheChildren.utils;

public enum PaymentType {
    PAYPAL,
    VISA,
    MASTERCARD,
    BITCOIN,
    AMERICAN_EXPRESS
}
