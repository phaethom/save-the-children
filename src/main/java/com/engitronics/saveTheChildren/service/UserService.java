package com.engitronics.saveTheChildren.service;

import com.engitronics.saveTheChildren.model.User;
import com.engitronics.saveTheChildren.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Page<User> retrieveAll(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    public User createOrUpdate(User user){

        return userRepository.save(user);
    }

    public Optional<User> retrieveById(long id){
        return userRepository.findById(id);
    }

    public void deleteById(long id){

        userRepository.deleteById(id);
    }
}
