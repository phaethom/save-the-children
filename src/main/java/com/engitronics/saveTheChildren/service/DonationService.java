package com.engitronics.saveTheChildren.service;

import com.engitronics.saveTheChildren.model.Donation;
import com.engitronics.saveTheChildren.repository.DonationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DonationService {

    @Autowired
    DonationRepository donationRepository;

    public Page<Donation> retrieveAll(Pageable pageable){
        return donationRepository.findAll(pageable);
    }

    public Donation createOrUpdate(Donation donation) {
        return donationRepository.save(donation);
    }
}