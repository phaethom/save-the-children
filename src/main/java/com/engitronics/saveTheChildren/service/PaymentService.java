package com.engitronics.saveTheChildren.service;

import com.engitronics.saveTheChildren.model.Payment;
import com.engitronics.saveTheChildren.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    public Page<Payment> retrieveAll(Pageable pageable){
        return paymentRepository.findAll(pageable);
    }

    public Payment createOrUpdate(Payment payment){
        return paymentRepository.save(payment);
    }

    public Optional<Payment> retrieveById(long id){
        return paymentRepository.findById(id);
    }

}
