package com.engitronics.saveTheChildren.configuration;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Save Children Service")
                .description("Save children service that allow user creation and persisted ")
                .version("0.0.1")
 //               .license("")
 //               .licenseUrl("")
                .contact(ContactInfo())
                .build();
    }

    private Contact ContactInfo() {
        return new Contact(
                "Save Children Support",
                "savethechildren.org.uk",
                "supportercare@savethechildren.org.uk");
    }
}
