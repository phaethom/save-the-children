package com.engitronics.saveTheChildren.repository;

import com.engitronics.saveTheChildren.model.Donation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonationRepository extends PagingAndSortingRepository<Donation,Long> {

    Page<Donation> findById(Long id, Pageable pageable);
}
