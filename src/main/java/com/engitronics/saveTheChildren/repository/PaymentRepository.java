package com.engitronics.saveTheChildren.repository;

import com.engitronics.saveTheChildren.model.Payment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends PagingAndSortingRepository<Payment, Long> {
}
