package com.engitronics.saveTheChildren.repository;

import com.engitronics.saveTheChildren.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User,Long> {


}
