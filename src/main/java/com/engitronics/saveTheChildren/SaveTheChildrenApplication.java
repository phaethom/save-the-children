package com.engitronics.saveTheChildren;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaveTheChildrenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaveTheChildrenApplication.class, args);
	}

}
