package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.exceptions.NotFoundException;
import com.engitronics.saveTheChildren.model.User;
import com.engitronics.saveTheChildren.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Something went wrong"),
        @ApiResponse(code = 401, message = "Unauthorised Operation"),
        @ApiResponse(code = 409, message = "Conflict with existing resource"),
        @ApiResponse(code = 403, message = "Access denied")})
public class UserController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "Create a new user")
    @PostMapping("/user")
    public ResponseEntity<?> create(@Valid @RequestBody User user, BindingResult result){
        user.setId(null);
        return new ResponseEntity<User>(userService.createOrUpdate(user), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Retrieve users")
    @GetMapping("/users")
    public ResponseEntity<Page<User>> getAllByPage(Pageable request){
        return new ResponseEntity<>(userService.retrieveAll(request), new HttpHeaders(), HttpStatus.OK);
    }

    @ApiOperation(value="Retrieve a single user")
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getById(@PathVariable long id) {
        User user = userService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("User with id %s not found!",id)));
        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @ApiOperation(value="Delete a user by id")
    @DeleteMapping("/user/{id}")
    public ResponseEntity<String> deleteById(@PathVariable long id) {
        userService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("User with id %s not found!",id)));
        userService.deleteById(id);
        return  new ResponseEntity<>("Deleted",HttpStatus.OK);
    }

    @ApiOperation(value="Update a user")
    @PutMapping("/user")
    public User updateUser(@RequestBody @Valid User user){
        return userService.createOrUpdate(user);
    }

}
