package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.model.Donation;
import com.engitronics.saveTheChildren.service.DonationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class DonationController {

    @Autowired
    private DonationService donationService;

    @ApiOperation(value = "Create single donation")
    @PostMapping("/donation")
    public ResponseEntity<?> create(@Valid @RequestBody Donation donation, BindingResult result){
        Donation donation1 = donationService.createOrUpdate(donation);
        return new ResponseEntity<Donation>(donation1, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Retrieve Donations")
    @GetMapping("/donations")
    public ResponseEntity<Page<Donation>> getAll(Pageable request){
        return new ResponseEntity<>(donationService.retrieveAll(request), new HttpHeaders(), HttpStatus.OK);
    }

}
