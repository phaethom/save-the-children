package com.engitronics.saveTheChildren.controller;

import com.engitronics.saveTheChildren.exceptions.NotFoundException;
import com.engitronics.saveTheChildren.model.Payment;
import com.engitronics.saveTheChildren.service.PaymentService;
import com.engitronics.saveTheChildren.utils.EncryptDecrypt;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Something went wrong"),
        @ApiResponse(code = 401, message = "Unauthorised Operation"),
        @ApiResponse(code = 409, message = "Conflict with existing resource"),
        @ApiResponse(code = 403, message = "Access denied")})
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @Autowired
    private EncryptDecrypt encryptDecrypt;

    @SneakyThrows
    @ApiOperation(value = "Create one payment")
    @PostMapping("/payment")
    public ResponseEntity<?> create(@Valid @RequestBody Payment payment, BindingResult result){

        String ivString = "1234567890123456";
        String secret = "HKlassd£5^()%$3";
        String cardNumber = payment.getPaymentTypeNumber();

        String cardNumberEncrypted = encryptDecrypt.encryptAes(cardNumber,ivString,secret);

        payment.setPaymentTypeNumber(cardNumberEncrypted);

        return new ResponseEntity<Payment>(paymentService.createOrUpdate(payment), HttpStatus.CREATED);
    }
    @ApiOperation(value = "retrieve payments")
    @GetMapping("/payments")
    public ResponseEntity<Page<Payment>> getallByPage(Pageable pageable){
        return new ResponseEntity<>(paymentService.retrieveAll(pageable), HttpStatus.OK);

    }

    @ApiOperation(value="Retrieve a single payment")
    @GetMapping("/payment/{id}")
    public ResponseEntity<Payment> getById(@PathVariable long id) {
        Payment payment = paymentService.retrieveById(id).orElseThrow(() -> new NotFoundException(String.format("Payment with id %s not found!",id)));
        return new ResponseEntity<Payment>(payment,HttpStatus.OK);
    }

}
