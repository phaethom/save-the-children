package com.engitronics.saveTheChildren.model;

import com.engitronics.saveTheChildren.utils.Constants;
import com.engitronics.saveTheChildren.utils.PaymentType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "payment")
@Data
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Float amount;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "donation_id", nullable = false)
    private Long donationId;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_type", nullable = false)
    private PaymentType paymentType;

    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "payment_type_number", nullable = false)
    private String paymentTypeNumber;

    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @Column(name = "expiry_date",nullable = false)
    private Date expiryDate;

    @PrePersist
    protected void onCreate(){
        this.createdOn = new Date();
    }

}
