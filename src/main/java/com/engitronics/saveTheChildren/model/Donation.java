package com.engitronics.saveTheChildren.model;

import com.engitronics.saveTheChildren.utils.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "donation")
public class Donation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String country;

    @JsonFormat(pattern = Constants.LOCAL_DATE_TIME_FORMAT)
    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @PrePersist
    protected void onCreate(){
        this.createdOn = new Date();
    }
}
