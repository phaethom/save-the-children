FROM openjdk:11.0.6-jre

RUN mkdir /app

#ADD build/libs/save-the-children-service.jar app/save-the-children-service.jar
ADD target/save-the-children-service.jar app/save-the-children-service.jar
ENTRYPOINT ["java","-jar","/app/save-the-children-service.jar"]