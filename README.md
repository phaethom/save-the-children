# Save children service 
---


###  Java REST API service with an end point that will be able to take a users information and then store that information ###

* Version 0.0.1-SNAPSHOT

## Deployment instructions
---
* Running the application from command line using Maven
1.In the base directory (containing pom.xml) run the following command:
 
  * `mvn clean install`
    
  * `mvn spring-boot:run`

#### Running application tests
  * `mvn test`

#### Running application without tests (in case tests are failing)
* `mvn -Dmaven.test.skip=true install`

#### Running as a Container
---
> Presuming that docker and docker-compose are installed

 
 1. In base directory where Dockerfile and docker-compose.yml are located run the command:
    
* Create jar:

with test
    * `mvn package` 
without test
    * `mvn package -DskipTests`
 2. Run both service and database containers

 * `docker-compose up`
   
 *  `docker-compose up -d` to run on background  

To stop and destroy the containers run the following command:

 * `docker-compose down`


  
#### Source Code 
---
[https://phaethom@bitbucket.org/phaethom/save-the-children.git](https://phaethom@bitbucket.org/phaethom/save-the-children.git)

#### Access Swagger documentation service
---
[swagger documentation](http://localhost:8087/swagger-ui.html)


### Author ###

* Odair Pires